// Поменять местами два числа без создания временной переменной.

package main

import "fmt"

func main() {
	a, b := 100, 200
	fmt.Println("a =", a, "b =", b)

	// 1 способ
	a, b = b, a
	fmt.Println("a =", a, "b =", b)

	// 2 способ
	a += b
	b = a - b
	a -= b
	fmt.Println("a =", a, "b =", b)
}
