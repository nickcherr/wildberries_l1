// Разработать программу, которая перемножает, делит, складывает, вычитает две
// числовых переменных a,b, значение которых > 2^20.

package main

import (
	"fmt"
	"math/big"
)

func main() {
	a := new(big.Int)
	a.SetString("2000000000000", 10)
	b := new(big.Int)
	b.SetString("1000000000000", 10)
	res := new(big.Int)

	fmt.Println("Произведение: ", res.Mul(a, b))
	fmt.Println("Частное: ", res.Quo(a, b))
	fmt.Println("Сумма: ", res.Add(a, b))
	fmt.Println("Разность: ", res.Sub(a, b))
}
