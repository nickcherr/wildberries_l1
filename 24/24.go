// Разработать программу нахождения расстояния между двумя точками, которые
// представлены в виде структуры Point с инкапсулированными параметрами x,y и
// конструктором.

package main

import (
	"fmt"
	"math"
)

type Point struct {
	x, y float64
}

func Distance(p1, p2 Point) float64 {
	dx := p1.x - p2.x
	dy := p1.y - p2.y
	return math.Sqrt(dx*dx + dy*dy)
}

func main() {
	p1 := Point{1, 1}
	p2 := Point{4, 6}
	fmt.Println("Расстояние между точками равно: ", Distance(p1, p2))
}
