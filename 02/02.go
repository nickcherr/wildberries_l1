// Написать программу, которая конкурентно рассчитает значение квадратов чисел
// взятых из массива (2,4,6,8,10) и выведет их квадраты в stdout.
package main

import (
	"fmt"
	"sync"
)

func countWithWorkerPool(nums []int) {
	workersNum := 3
	var wg sync.WaitGroup
	wg.Add(workersNum)
	ch := make(chan int)

	for i := 0; i < workersNum; i++ {
		go func() {
			for value := range ch {
				fmt.Println(value * value)
			}
			wg.Done()
		}()
	}

	for _, num := range nums {
		ch <- num
	}

	close(ch)
	wg.Wait()
}

func countWithGoRountines(nums []int) {
	var wg sync.WaitGroup

	for _, num := range nums {
		wg.Add(1)
		go func(value int) {
			fmt.Println(value * value)
			wg.Done()
		}(num)
	}

	wg.Wait()
}

func main() {
	nums := []int{2, 4, 6, 8, 10}
	countWithGoRountines(nums)
	fmt.Println()
	countWithWorkerPool(nums)
}
