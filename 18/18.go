// Реализовать структуру-счетчик, которая будет инкрементироваться в
// конкурентной среде. По завершению программа должна выводить итоговое
// значение счетчика.

package main

import (
	"fmt"
	"sync"
)

type Counter struct {
	mu    sync.Mutex
	value int
}

func main() {
	numGoRoutines := 5
	var cnt Counter
	var wg sync.WaitGroup

	wg.Add(numGoRoutines)
	for i := 0; i < numGoRoutines; i++ {
		go func() {
			cnt.mu.Lock()
			cnt.value++
			cnt.mu.Unlock()
			wg.Done()
		}()
	}

	wg.Wait()
	fmt.Println(cnt.value)
}
