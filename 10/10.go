// Дана последовательность температурных колебаний: -25.4, -27.0 13.0, 19.0,
// 15.5, 24.5, -21.0, 32.5. Объединить данные значения в группы с шагом в 10
// градусов. Последовательность в подмножноствах не важна.
// Пример: -20:{-25.0, -27.0, -21.0}, 10:{13.0, 19.0, 15.5}, 20: {24.5}, etc.

package main

import (
	"fmt"
	"strconv"
)

func main() {
	temps := []float64{-25.4, -27.0, 13.0, 19.0, 15.5, 24.5, -21.0, 32.5}
	m := make(map[string][]float64)

	for _, temp := range temps {
		key := strconv.Itoa(int(temp) / 10 * 10)
		if key == "0" && temp < 0 {
			key = "-0"
		}

		group := m[key]
		group = append(group, temp)
		m[key] = group
	}

	for k, v := range m {
		fmt.Println(k, ":", v)
	}
}
