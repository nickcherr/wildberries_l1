// Реализовать паттерн «адаптер» на любом примере.

package main

import "fmt"

type CarAdapter struct {
	*Truck
}

func NewAdapter(adaptee *Truck) Car {
	return &CarAdapter{adaptee}
}

type Car interface {
	Info()
}

type Truck struct{}

func (adaptee *Truck) AdaptedInfo() {
	fmt.Println("I am truck")
}

func (adapter *CarAdapter) Info() {
	adapter.AdaptedInfo()
}

func main() {
	adapter := NewAdapter(&Truck{})
	adapter.Info()
}
