// Разработать программу, которая переворачивает слова в строке.
// Пример: «snow dog sun — sun dog snow».

package main

import (
	"fmt"
	"strings"
)

func main() {
	s := "snow dog sun"
	fmt.Println(reverseWords(s))
}

func reverseWords(s string) string {
	words := strings.Split(s, " ")
	if len(words) == 1 {
		return s
	}
	var b strings.Builder
	for i := len(words) - 1; i > 0; i-- {
		b.WriteString(words[i] + " ")
	}
	b.WriteString(words[0])

	return b.String()
}
