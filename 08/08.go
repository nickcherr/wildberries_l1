// Дана переменная int64. Разработать программу которая устанавливает i-й бит в
// 1 или 0.

package main

import (
	"fmt"
	"strconv"
)

// нумерация битов справа налево, начиная с нулевого
// bitValue = 0, 1
// bitIndex = 0, 1, 2, ... , 63
func setBit(variable *int64, bitValue int, bitIndex int) {
	if bitValue != 0 && bitValue != 1 && (bitIndex > 63 || bitIndex < 0) {
		return
	}
	temp := *variable
	logicValue := int64(1 << bitIndex)
	if bitValue == 0 {
		temp = temp & ^logicValue
	} else {
		temp = temp | logicValue
	}
	*variable = temp
}

func main() {
	var n int64 = 255
	fmt.Println(strconv.FormatInt(n, 2))
	setBit(&n, 0, 5)
	fmt.Println(strconv.FormatInt(n, 2))
	fmt.Println()

	n = 128
	fmt.Println(strconv.FormatInt(n, 2))
	setBit(&n, 1, 5)
	fmt.Println(strconv.FormatInt(n, 2))
}
