// Реализовать все возможные способы остановки выполнения горутины.

package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	// 1. С помощью специального канала, в который посылается сообщение об остановке
	ch := make(chan bool)
	go func() {
		for {
			select {
			case <-ch:
				fmt.Println("1 stopped from channel")
				return
			default:
				time.Sleep(1 * time.Second)
			}
		}
	}()

	// 2. С помощью таймера
	go func() {
		time.Sleep(3 * time.Second)
		fmt.Println("2 stopped after time")
	}()

	// 3. С помощью сигнала прерывания (CTRL+C)
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sigs
		fmt.Println("3 stopped with signal")
	}()

	time.Sleep(5 * time.Second)
	ch <- true
}
