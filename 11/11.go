// Реализовать пересечение двух неупорядоченных множеств.

package main

import "fmt"

func main() {
	nums1 := []int{12, 14, 10, 7}
	nums2 := []int{14, 8, 6, 12, 16}

	fmt.Println(intersection(nums1, nums2))
}

func intersection(nums1, nums2 []int) []int {
	if len(nums1)*len(nums2) == 0 {
		return []int{}
	}

	res := []int{}
	m := map[int]int{}

	for _, num := range nums1 {
		m[num] = 1
	}

	for _, num := range nums2 {
		val, ok := m[num]

		// not in nums1
		if !ok {
			continue
		}

		// val == 1 -> in nums1 and not in res
		// val == 2 -> in nums1 and in res
		if val == 1 {
			m[num] = 2
			res = append(res, num)
		}
	}

	return res
}
