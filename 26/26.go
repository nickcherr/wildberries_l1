// Разработать программу, которая проверяет, что все символы в строке
// уникальные (true — если уникальные, false etc). Функция проверки должна быть
// регистронезависимой.

package main

import (
	"fmt"
	"strings"
)

func main() {
	// s := "abcd" // true
	// s := "abCdefAaf" // false
	// s := "aabcd" // false
	s := "aAb" // false

	fmt.Println(unique(s))
}

func unique(s string) bool {
	if len([]rune(s)) <= 1 {
		return true
	}
	m := map[rune]struct{}{}
	for _, ch := range strings.ToLower(s) {
		if _, ok := m[ch]; ok {
			return false
		}
		m[ch] = struct{}{}
	}
	return true
}
