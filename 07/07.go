// Реализовать конкурентную запись данных в map.

package main

import (
	"fmt"
	"sync"
)

func main() {
	m := map[int]int{}
	wg := sync.WaitGroup{}
	numWriters := 5
	var mu sync.Mutex

	for i := 0; i <= numWriters; i++ {
		wg.Add(1)
		go func(num int) {
			mu.Lock()
			m[num] = num
			mu.Unlock()
			wg.Done()
		}(i)
	}

	wg.Wait()
	fmt.Println(m)
}
