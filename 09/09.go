// Разработать конвейер чисел. Даны два канала: в первый пишутся числа (x) из
// массива, во второй — результат операции x*2, после чего данные из второго
// канала должны выводиться в stdout.

package main

import (
	"fmt"
	"sync"
)

func main() {
	wg := new(sync.WaitGroup)
	chan1 := make(chan int)
	chan2 := make(chan int)
	nums := []int{2, 4, 6, 8, 10}

	wg.Add(2)
	go func() {
		var dbl int
		for num := range chan1 {
			dbl = num * 2
			chan2 <- dbl
		}
		wg.Done()
		close(chan2)
	}()

	go func() {
		for res := range chan2 {
			fmt.Println(res)
		}
		wg.Done()
	}()

	for _, num := range nums {
		chan1 <- num
	}

	close(chan1)
	wg.Wait()
}
