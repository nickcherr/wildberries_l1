// Реализовать бинарный поиск встроенными методами языка.

package main

import "fmt"

func main() {
	nums := []int{1, 2, 3, 4, 5, 6, 7}
	fmt.Println(binarySearch(2, nums))
}

func binarySearch(lookingFor int, arr []int) int {
	min := 0
	max := len(arr) - 1

	for min <= max {
		mid := min + (max-min)/2

		if arr[mid] == lookingFor {
			return mid
		} else if arr[mid] < lookingFor {
			min = mid + 1
		} else {
			max = mid - 1
		}
	}

	return -1
}
