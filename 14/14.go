// Разработать программу, которая в рантайме способна определить тип
// переменной: int, string, bool, channel из переменной типа interface{}.

package main

import (
	"fmt"
	"reflect"
)

func main() {
	var num int
	var str string
	var boolean bool
	var channel chan int

	printType(num)
	printType(str)
	printType(boolean)
	printType(channel)
}

func printType(a interface{}) {
	fmt.Println(reflect.TypeOf(a))
}
