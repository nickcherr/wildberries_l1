// К каким негативным последствиям может привести данный фрагмент кода, и как
// это исправить? Приведите корректный пример реализации.
// var justString string
// func someFunc() {
// 	v := createHugeString(1 << 10)
// 	justString = v[:100]
// }
// func main() {
// 	someFunc()
// }

package main

import (
	"fmt"
	"strings"
)

var justString string

func createHugeString(size int) string {
	return strings.Repeat("1", size)
}

func someFunc() {
	// Если аргумент функции createHugeString обозначает длину создаваемой строки,
	// то лучше сразу создать строку нужной длины 100 и положить её в переменную justString,
	// так как выделять большой объем памяти лишний раз неэффективно, а также justString будет
	// ссылаться на кусок памяти из созданной большой строки и та не будет удалена сборщиком мусора.
	justString = createHugeString(100)

	// Помимо того в зависимости от реализации createHugeString могла бы вернуться строка длиной меньше 100,
	// тогда была бы ошибка. Также если символы не только однобайтовые, то 100 - это длина в байтах,
	// а символов в строке будет меньше.
}

func main() {
	someFunc()
	fmt.Println(justString)
}
