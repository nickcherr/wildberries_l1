// Дана структура Human (с произвольным набором полей и методов).
// Реализовать встраивание методов в структуре Action от родительской структуры
// Human (аналог наследования).

package main

import "fmt"

type Human struct {
	Name string
	Age  int
}

type Action struct {
	Hobby string
	Human
}

func (h Human) printInfo() {
	fmt.Printf("My name is %s, I'm %d years old\n", h.Name, h.Age)
}

func main() {
	action := Action{
		Hobby: "Football",
		Human: Human{
			Name: "Alex",
			Age:  10,
		},
	}

	action.printInfo()
}
