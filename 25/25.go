// Реализовать собственную функцию sleep.

package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("Start sleeping")
	sleep(1 * time.Second)
	fmt.Println("Stop sleeping")
}

func sleep(d time.Duration) {
	<-time.After(d)
}
