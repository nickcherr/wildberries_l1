// Имеется последовательность строк - (cat, cat, dog, cat, tree) создать для нее
// собственное множество.

package main

import "fmt"

func main() {
	words := []string{"cat", "cat", "dog", "cat", "tree"}
	m := make(map[string]struct{})
	set := []string{}

	for _, word := range words {
		m[word] = struct{}{}
	}

	for key := range m {
		set = append(set, key)
	}

	fmt.Println(set)
}
