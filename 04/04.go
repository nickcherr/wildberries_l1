// Реализовать постоянную запись данных в канал (главный поток). Реализовать
// набор из N воркеров, которые читают произвольные данные из канала и
// выводят в stdout. Необходима возможность выбора количества воркеров при
// старте.
// Программа должна завершаться по нажатию Ctrl+C. Выбрать и обосновать
// способ завершения работы всех воркеров.

package main

import (
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

var mu sync.Mutex

func writeToChannel(stop, ch chan int) {
	i := 0
	for {
		select {
		case <-stop:
			return
		default:
			ch <- i
			i++
		}
	}
}

func worker(ch chan int, wg *sync.WaitGroup) {
	defer wg.Done()
	for data := range ch {
		mu.Lock()
		fmt.Println(data)
		mu.Unlock()
	}
}

func main() {
	ch := make(chan int)
	stop := make(chan int)
	var wg sync.WaitGroup
	var workersNum int
	_, _ = fmt.Scanf("%d", &workersNum)

	go writeToChannel(stop, ch)

	wg.Add(workersNum)
	for i := 0; i < workersNum; i++ {
		go worker(ch, &wg)
	}

	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)

	<-signals
	stop <- 1
	close(ch)
	wg.Wait()
}
