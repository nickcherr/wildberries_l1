// Разработать программу, которая будет последовательно отправлять значения в
// канал, а с другой стороны канала — читать. По истечению N секунд программа
// должна завершаться.

package main

import (
	"fmt"
	"time"
)

func main() {
	ch := make(chan int)
	go writeToChannel(ch)

	timeout := 1 * time.Second
	timer := time.NewTimer(timeout)

	for {
		select {
		case data := <-ch:
			fmt.Println(data)
		case <-timer.C:
			fmt.Println("Time is over")

			return
		}
	}
}

func writeToChannel(ch chan int) {
	i := 0
	for {
		ch <- i
		i++
	}
}
