// Дана последовательность чисел: 2,4,6,8,10. Найти сумму их
// квадратов(2^2+3^2+4^2….) с использованием конкурентных вычислений.

package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

func countWithMutex(nums []int) {
	var wg sync.WaitGroup
	var sum int
	var mu = &sync.Mutex{}

	for _, num := range nums {
		wg.Add(1)
		go func(value int) {
			mu.Lock()
			sum += value * value
			mu.Unlock()
			wg.Done()
		}(num)
	}

	wg.Wait()
	fmt.Println(sum)
}

func countWithAtomic(nums []int) {
	var wg sync.WaitGroup
	var sum int32

	for _, num := range nums {
		wg.Add(1)
		go func(value int) {
			atomic.AddInt32(&sum, int32(value*value))
			wg.Done()
		}(num)
	}

	wg.Wait()
	fmt.Println(sum)
}

func main() {
	nums := []int{2, 4, 6, 8, 10}
	countWithMutex(nums)
	countWithAtomic(nums)
}
