// Удалить i-ый элемент из слайса.

package main

import "fmt"

func main() {
	s := []int{1, 2, 3, 4, 5}
	fmt.Println(removeFromSlice(s, 2))
}

func removeFromSlice(s []int, i int) []int {
	return append(s[:i], s[i+1:]...)
}
